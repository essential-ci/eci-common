package pl.jdudycz.eci.common.kafka.serialization

import com.google.protobuf.InvalidProtocolBufferException
import org.apache.kafka.common.serialization.Deserializer
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.util.logger

class EventDeserializer : Deserializer<Event> {

    override fun deserialize(topic: String, data: ByteArray): Event? = try {
        Event.parseFrom(data)
    } catch (e: InvalidProtocolBufferException) {
        log.warn("Failed to parse event", e)
        null
    }

    companion object {
        val log = logger<EventDeserializer>()
    }
}
