package pl.jdudycz.eci.common.kafka.core

import com.google.protobuf.ProtocolMessageEnum
import org.springframework.stereotype.Component
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event

@Component
internal class TopicRouter {
    fun resolveTopicName(event: Event): String =
        event.type.toLower()
            .split("_")
            .joinToString(".")
            .let { "$BASE_TOPIC_NAME.${it}" }

    private fun ProtocolMessageEnum.toLower() = this.toString().toLowerCase()

    companion object {
        private const val BASE_TOPIC_NAME = "eci"
    }
}
