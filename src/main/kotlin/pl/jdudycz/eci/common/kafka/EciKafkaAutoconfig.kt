package pl.jdudycz.eci.common.kafka

import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.kafka.core.EventPublisherImpl
import pl.jdudycz.eci.common.kafka.core.TopicRouter
import pl.jdudycz.eci.common.kafka.serialization.EventSerializer
import reactor.kafka.sender.SenderOptions


@Configuration
@ComponentScan
@EnableConfigurationProperties(KafkaProperties::class)
internal class EciKafkaAutoconfig {

    @Bean
    fun eventPublisher(kafkaProps: KafkaProperties, router: TopicRouter): EventPublisher {
        val props = kafkaProps.buildProducerProperties()
        val senderOpts = SenderOptions.create<String, Event>(props)
            .withKeySerializer(StringSerializer())
            .withValueSerializer(EventSerializer())
        return EventPublisherImpl(ReactiveKafkaProducerTemplate(senderOpts), router)
    }
}
