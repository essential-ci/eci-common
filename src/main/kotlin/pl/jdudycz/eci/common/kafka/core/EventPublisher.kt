package pl.jdudycz.eci.common.kafka.core

import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderResult

interface EventPublisher {
    fun publish(events: Flux<Event>): Flux<String>

    fun publish(event: Mono<Event>): Mono<String>
}
