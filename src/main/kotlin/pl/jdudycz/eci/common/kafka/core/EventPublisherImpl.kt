package pl.jdudycz.eci.common.kafka.core

import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kafka.sender.SenderRecord

internal class EventPublisherImpl(
    private val producer: ReactiveKafkaProducerTemplate<String, Event>,
    private val router: TopicRouter
) : EventPublisher {

    override fun publish(events: Flux<Event>): Flux<String> =
        events
            .map(this::createRecord)
            .transform { producer.send(it) }
            .doOnNext { log.trace("Event published to topic ${it.recordMetadata().topic()}") }
            .map { it.correlationMetadata() }

    override fun publish(event: Mono<Event>): Mono<String> = publish(event.flux()).single()

    private fun createRecord(event: Event): SenderRecord<String, Event, String> =
        router
            .resolveTopicName(event)
            .let { ProducerRecord(it, event.eventId, event) }
            .let { SenderRecord.create(it, event.correlationId) }

    companion object {
        val log = logger<EventPublisherImpl>()
    }
}
