package pl.jdudycz.eci.common.kafka.core

import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.util.logger
import reactor.core.publisher.Flux


internal class EventConsumerImpl(private val consumer: ReactiveKafkaConsumerTemplate<String, Event>) : EventConsumer {

    override fun consume(): Flux<Event> =
        consumer.receiveAutoAck()
            .filter { it.value() != null }
            .doOnNext { log.trace("Event ${it.value().eventId} received from ${it.topic()}") }
            .map { it.value() }

    companion object {
        val log = logger<EventConsumerImpl>()
    }
}
