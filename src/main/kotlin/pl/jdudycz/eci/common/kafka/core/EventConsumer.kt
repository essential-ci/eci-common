package pl.jdudycz.eci.common.kafka.core

import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.kafka.serialization.EventDeserializer
import reactor.core.publisher.Flux
import reactor.kafka.receiver.ReceiverOptions


interface EventConsumer {
    fun consume(): Flux<Event>

    companion object {
        fun create(topic: String, props: KafkaProperties): EventConsumer {
            val receiverOpts = ReceiverOptions.create<String, Event>(props.buildConsumerProperties())
                .withKeyDeserializer(StringDeserializer())
                .withValueDeserializer(EventDeserializer())
                .subscription(listOf(topic))
            return EventConsumerImpl(ReactiveKafkaConsumerTemplate(receiverOpts))
        }
    }
}
