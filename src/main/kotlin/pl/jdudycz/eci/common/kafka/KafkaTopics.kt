package pl.jdudycz.eci.common.kafka

const val TOPIC_PREFIX = "eci."

const val SPACE_SAVE_TOPIC = "eci.space.save"
const val SPACE_SAVE_SUCCESS_TOPIC = "eci.space.save.success"
const val SPACE_SAVE_FAILED_TOPIC = "eci.space.save.failed"
const val SPACE_REMOVE_TOPIC = "eci.space.remove"
const val SPACE_REMOVED_TOPIC = "eci.space.removed"

const val SPACE_ACCESS_GRANT_TOPIC = "eci.space.access.grant"
const val SPACE_ACCESS_GRANT_SUCCESS_TOPIC = "eci.space.access.grant.success"
const val SPACE_ACCESS_GRANT_FAILED_TOPIC = "eci.space.access.grant.failed"
const val SPACE_ACCESS_REMOVE_TOPIC = "eci.space.access.remove"
const val SPACE_ACCESS_REMOVED_TOPIC = "eci.space.access.removed"

const val AGENT_REGISTER_TOPIC = "eci.agent.register"
const val AGENT_REGISTER_SUCCESS_TOPIC = "eci.agent.register.success"
const val AGENT_REGISTER_FAILED_TOPIC = "eci.agent.register.failed"
const val AGENT_UNREGISTER_TOPIC = "eci.agent.unregister"
const val AGENT_UNREGISTERED_TOPIC = "eci.agent.unregistered"
const val AGENT_AVAILABLE_TOPIC = "eci.agent.available"
const val AGENT_UNAVAILABLE_TOPIC = "eci.agent.unavailable"

const val PROJECT_SAVE_TOPIC = "eci.project.save"
const val PROJECT_SAVE_SUCCESS_TOPIC = "eci.project.save.success"
const val PROJECT_SAVE_FAILED_TOPIC = "eci.project.save.failed"
const val PROJECT_REMOVE_TOPIC = "eci.project.remove"
const val PROJECT_REMOVED_TOPIC = "eci.project.removed"

const val JOB_CREATE_TOPIC = "eci.job.create"
const val JOB_CREATE_SUCCESS_TOPIC = "eci.job.create.success"
const val JOB_CREATE_FAILED_TOPIC = "eci.job.create.failed"
const val JOB_ACCEPTED_TOPIC = "eci.job.accepted"
const val JOB_CANCEL_TOPIC = "eci.job.cancel"
const val JOB_CANCELLED_TOPIC = "eci.job.cancelled"
const val JOB_FAILED_TOPIC = "eci.job.failed"
const val JOB_SUCCEEDED_TOPIC = "eci.job.succeeded"
const val JOB_STARTED_TOPIC = "eci.job.started"

const val TASK_SUCCEEDED_TOPIC = "eci.task.succeeded"
const val TASK_FAILED_TOPIC = "eci.task.failed"
const val TASK_STARTED_TOPIC = "eci.task.started"
const val TASK_OUTPUT_TOPIC = "eci.task.output"

const val REPO_CHANGED_TOPIC = "eci.repo.changed"
