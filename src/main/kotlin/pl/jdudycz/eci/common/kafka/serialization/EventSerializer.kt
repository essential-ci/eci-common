package pl.jdudycz.eci.common.kafka.serialization

import org.apache.kafka.common.serialization.Serializer
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event

class EventSerializer : Serializer<Event> {
    override fun serialize(topic: String, data: Event): ByteArray {

        return data.toByteArray()
    }
}
