package pl.jdudycz.eci.common.domain.event

import com.google.protobuf.ByteString
import pl.jdudycz.eci.common.domain.Space
import java.util.*

fun spaceSave(data: Space.SpaceData) = createEvent(
    EventOuterClass.EventType.SPACE_SAVE,
    data.toByteString(),
    idGenerator.generate()
)

fun spaceSaveSuccess(correlationId: UUID, id: String, data: Space.SpaceData) = createEvent(
    EventOuterClass.EventType.SPACE_SAVE_SUCCESS,
    data.toBuilder().setId(id).build().toByteString(),
    correlationId
)

fun spaceSaveFailed(correlationId: UUID, data: Space.SpaceData, errorMessage: String) = createEvent(
    EventOuterClass.EventType.SPACE_SAVE_FAILED,
    Space.SpaceCreateFailure.newBuilder()
        .setData(data)
        .setErrorMessage(errorMessage)
        .build().toByteString(),
    correlationId
)

fun spaceRemove(id: String) = createEvent(
    EventOuterClass.EventType.SPACE_REMOVE,
    ByteString.copyFromUtf8(id),
    idGenerator.generate()
)

fun spaceRemoved(correlationId: UUID, id: String) = createEvent(
    EventOuterClass.EventType.SPACE_REMOVED,
    ByteString.copyFromUtf8(id),
    correlationId
)

fun accessGrant(spaceId: String, userId: String, role: Space.SpaceRole) = createEvent(
    EventOuterClass.EventType.SPACE_ACCESS_GRANT,
    Space.SpaceRoleMapping.newBuilder()
        .setSpaceId(spaceId)
        .setUserId(userId)
        .setRole(role)
        .build().toByteString()
)

fun accessRemove(spaceId: String, userId: String) = createEvent(
    EventOuterClass.EventType.SPACE_ACCESS_REMOVE,
    Space.SpaceAccessRemoveRequest.newBuilder()
        .setSpaceId(spaceId)
        .setUserId(userId)
        .build().toByteString(),
)
