package pl.jdudycz.eci.common.domain.event

import com.fasterxml.uuid.Generators
import com.fasterxml.uuid.impl.TimeBasedGenerator
import com.google.protobuf.ByteString
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event.newBuilder
import pl.jdudycz.eci.common.domain.event.EventOuterClass.EventType
import java.util.*

val idGenerator: TimeBasedGenerator = Generators.timeBasedGenerator()

internal fun createEvent(eventType: EventType, data: ByteString, correlationId: UUID? = null): Event =
    newBuilder()
        .setEventId(idGenerator.generate().toString())
        .setCorrelationId(correlationId?.toString() ?: "")
        .setType(eventType)
        .setTimestamp(System.currentTimeMillis())
        .setData(data)
        .build()
