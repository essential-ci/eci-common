package pl.jdudycz.eci.common.domain.event

import com.google.protobuf.ByteString
import pl.jdudycz.eci.common.domain.Job.*
import pl.jdudycz.eci.common.domain.event.EventOuterClass.EventType
import java.util.*

fun jobCreate(data: JobData) = createEvent(
    EventType.JOB_CREATE,
    data.toByteString(),
    idGenerator.generate()
)

fun jobCreated(correlationId: UUID, id: String, requestData: JobData, tasks: List<PipelineTask>) = createEvent(
    EventType.JOB_CREATE_SUCCESS,
    requestData.toBuilder()
        .setId(id)
        .clearTasks()
        .addAllTasks(tasks)
        .build().toByteString(),
    correlationId
)

fun jobCreateFailed(correlationId: UUID, errorMessage: String, requestData: JobData) = createEvent(
    EventType.JOB_CREATE_FAILED,
    JobCreateFailure.newBuilder()
        .setData(requestData)
        .setErrorMessage(errorMessage)
        .build().toByteString(),
    correlationId
)

fun jobAccepted(jobId: String, agentId: String) = createEvent(
    EventType.JOB_ACCEPTED,
    AgentsJob.newBuilder()
        .setAgentId(agentId)
        .setJobId(jobId)
        .build().toByteString()
)

fun jobStarted(id: String) = createEvent(EventType.JOB_STARTED, ByteString.copyFromUtf8(id))

fun jobSucceeded(id: String) = createEvent(EventType.JOB_SUCCEEDED, ByteString.copyFromUtf8(id))

fun jobCancel(id: String) = createEvent(EventType.JOB_CANCEL, ByteString.copyFromUtf8(id))

fun jobCancelled(id: String) = createEvent(EventType.JOB_CANCELLED, ByteString.copyFromUtf8(id))

fun jobFailed(jobId: String, errorMessage: String) = createEvent(
    EventType.JOB_FAILED,
    JobFailure.newBuilder()
        .setJobId(jobId)
        .setErrorMessage(errorMessage)
        .build().toByteString()
)

fun taskStarted(taskId: String) = createEvent(
    EventType.TASK_STARTED,
    ByteString.copyFromUtf8(taskId)
)

fun taskFailed(taskId: String) = createEvent(
    EventType.TASK_FAILED,
    ByteString.copyFromUtf8(taskId)
)

fun taskSucceeded(taskId: String) = createEvent(
    EventType.TASK_SUCCEEDED,
    ByteString.copyFromUtf8(taskId)
)

fun taskOutput(taskId: String, content: String) = createEvent(
    EventType.TASK_OUTPUT,
    TaskOutput.newBuilder()
        .setTaskId(taskId)
        .setContent(content)
        .build().toByteString()
)
