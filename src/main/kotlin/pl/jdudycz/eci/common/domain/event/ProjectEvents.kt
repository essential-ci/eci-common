package pl.jdudycz.eci.common.domain.event

import com.google.protobuf.ByteString
import pl.jdudycz.eci.common.domain.Project
import pl.jdudycz.eci.common.domain.event.EventOuterClass.EventType
import java.util.*

fun projectSave(data: Project.ProjectData) = createEvent(
    EventType.PROJECT_SAVE,
    data.toByteString(),
    idGenerator.generate()
)

fun projectSaveSuccess(correlationId: UUID, id: String, data: Project.ProjectData) = createEvent(
    EventType.PROJECT_SAVE_SUCCESS,
    data.toBuilder().setId(id).build().toByteString(),
    correlationId
)

fun projectSaveFailed(correlationId: UUID, errorMessage: String, data: Project.ProjectData) = createEvent(
    EventType.PROJECT_SAVE_FAILED,
    Project.ProjectCreateFailure.newBuilder()
        .setErrorMessage(errorMessage)
        .setData(data)
        .build().toByteString(),
    correlationId
)

fun projectRemove(id: String) = createEvent(
    EventType.PROJECT_REMOVE,
    ByteString.copyFromUtf8(id),
    idGenerator.generate()
)

fun projectRemoved(correlationId: UUID, id: String) = createEvent(
    EventType.PROJECT_REMOVED,
    ByteString.copyFromUtf8(id),
    correlationId
)
