package pl.jdudycz.eci.common.domain.event

import pl.jdudycz.eci.common.domain.Repo

fun repoChanged(data: Repo.ChangeData) = createEvent(
    EventOuterClass.EventType.REPO_CHANGED,
    data.toByteString(),
)
