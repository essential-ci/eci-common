package pl.jdudycz.eci.common.domain.event

import com.google.protobuf.ByteString
import pl.jdudycz.eci.common.domain.Agent.*
import pl.jdudycz.eci.common.domain.event.EventOuterClass.EventType
import java.util.*

fun agentRegister(registrationCode: String, host: String, name: String, apiKey: String) = createEvent(
    EventType.AGENT_REGISTER,
    AgentRegistrationRequest.newBuilder()
        .setRegistrationCode(registrationCode)
        .setHost(host)
        .setName(name)
        .setApiKey(apiKey)
        .build().toByteString(),
    idGenerator.generate()
)

fun agentRegisterSuccess(correlationId: UUID, id: String, request: AgentRegistrationRequest) = createEvent(
    EventType.AGENT_REGISTER_SUCCESS,
    AgentData.newBuilder()
        .setId(id)
        .setHost(request.host)
        .setName(request.name)
        .build().toByteString(),
    correlationId
)

fun agentRegisterFailed(correlationId: UUID, errorMessage: String, request: AgentRegistrationRequest) = createEvent(
    EventType.AGENT_REGISTER_FAILED,
    AgentRegistrationFailure.newBuilder()
        .setData(request)
        .setErrorMessage(errorMessage)
        .build().toByteString(),
    correlationId
)

fun agentUnregister(id: String) = createEvent(
    EventType.AGENT_UNREGISTER,
    ByteString.copyFromUtf8(id),
    idGenerator.generate()
)

fun agentUnregistered(correlationId: UUID, id: String) = createEvent(
    EventType.AGENT_UNREGISTERED,
    ByteString.copyFromUtf8(id),
    correlationId
)

fun agentAvailable(id: String) = createEvent(
    EventType.AGENT_AVAILABLE,
    ByteString.copyFromUtf8(id),
)

fun agentUnavailable(id: String) = createEvent(
    EventType.AGENT_UNAVAILABLE,
    ByteString.copyFromUtf8(id),
)
