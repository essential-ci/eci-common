package pl.jdudycz.eci.common.util

fun Map<String, Any>.asEnvString() = map { "${it.key}=${it.value}" }

fun List<String>.asEnvMap(): Map<String, Any> =
    map { it.split("=") }.map { (key, value) -> key to value }.toMap()
