package pl.jdudycz.eci.common.util.encryption

import org.springframework.security.crypto.encrypt.Encryptors

class TextEncryptor(password: String, salt: String) {
    private val encryptor = Encryptors.text(password, salt)

    fun encrypt(string: String): String = encryptor.encrypt(string)

    fun decrypt(string: String): String = encryptor.decrypt(string)
}
