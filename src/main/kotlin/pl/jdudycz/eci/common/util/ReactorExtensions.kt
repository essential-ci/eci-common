package pl.jdudycz.eci.common.util

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty


fun <T> Mono<T>.resumeOnError(fallback: (e: Throwable) -> Unit): Mono<T> =
    onErrorResume {
        fallback(it)
        Mono.empty()
    }

fun <T> Flux<T>.resumeOnError(fallback: (e: Throwable) -> Unit): Flux<T> =
    onErrorResume {
        fallback(it)
        Mono.empty()
    }

fun <T> Flux<T>.doOnEmpty(callback: () -> Unit) = switchIfEmpty {
    callback()
    Flux.empty<T>()
}

fun <T> Mono<T>.doOnEmpty(callback: () -> Unit) = switchIfEmpty {
    callback()
    Mono.empty<T>()
}
