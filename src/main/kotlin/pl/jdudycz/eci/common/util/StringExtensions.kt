package pl.jdudycz.eci.common.util

import java.util.*

fun String.toUUID(): UUID = UUID.fromString(this)
