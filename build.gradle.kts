import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc
import pl.allegro.tech.build.axion.release.domain.VersionConfig

plugins {
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.spring") version "1.4.0"
    kotlin("kapt") version "1.4.0"
    id("idea")
    id("java-library")
    id("maven-publish")
    id("com.google.protobuf") version "0.8.12"
    id("org.springframework.boot") version "2.3.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("pl.allegro.tech.build.axion-release") version "1.12.0"
}

configure<VersionConfig> {
    versionIncrementer("incrementMinor")
}

group = "pl.jdudycz.eci"
version = scmVersion.version
java.sourceCompatibility = JavaVersion.VERSION_11


val protobufVersion = "3.12.2"
val uuidVersion = "4.0.1"

repositories {
    mavenCentral()
}

dependencies {
    api("com.google.protobuf:protobuf-java:${protobufVersion}")
    api("io.projectreactor.kafka:reactor-kafka")
    api("io.projectreactor.addons:reactor-extra")
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.security:spring-security-crypto")
    implementation("org.springframework.boot:spring-boot-configuration-processor")
    implementation("org.springframework.kafka:spring-kafka")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.fasterxml.uuid:java-uuid-generator:${uuidVersion}")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("io.projectreactor:reactor-test")
}

publishing {
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/19729725/packages/maven")
            name = "eci-common"
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = System.getenv("GITLAB_DEPLOY_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
    publications {
        create<MavenPublication>(project.name) {
            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()
            from(components["java"])
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${protobufVersion}"
    }
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

tasks {
    test {
        useJUnitPlatform()
    }
    compileKotlin {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }
    jar {
        enabled = true
    }
    bootJar {
        enabled = false
    }
}
